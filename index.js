function countLetter(letter, sentence) {
    let result = 0;
    // Check first whether the letter is a single character.
    if (letter.length > 1){
        return undefined
    //single letter
    } else {
        for(let p = 0; p <= sentence.length; p++){
            //Conditons:
                // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
                // If letter is invalid, return undefined.
            if (sentence[p] == letter){
                result += 1;
                //console.log(result);
            }
        }
        return result;
    }
}


function isIsogram(text) {
    //Check:
        // The function should disregard text casing before doing anything else.
    let isogramWord = text.toLowerCase();
    let compare;
    //Goal:
        // An isogram is a word where there are no repeating letters.
    for (let d = 0; d <= isogramWord.length; d++){
    
    //Condition:
        // If the function finds a repeating letter, return false. Otherwise, return true.
        if ((isogramWord[d] === compare) && d > 1){
            return false;
        }
        compare = isogramWord[d];

    }
    return true;
    
}

function purchase(age, price) {

    //Check:
        // The returned value should be a string.


    //Conditions:
    // Return undefined for people aged below 13.
    if (age < 13){
        return undefined;
    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    } else if ( (age >= 13 && age <= 21) || age > 65 ){
        return (price * 0.8).toFixed(2);
    // Return the rounded off price for people aged 22 to 64.
    } else if ( age >= 22 && age <= 64 ){
        return price.toFixed(2);
    }
}

function findHotCategories(items) {
    //Goal:
        // Find categories that has no more stocks.
        // The hot categories must be unique; no repeating categories.

    //Array for the test:
        // The passed items array from the test are the following:
        // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
        // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
        // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
        // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
        // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }


    //Expected return must be array:
        // The expected output after processing the items array is ['toiletries', 'gadgets'].

    // Note:
        // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.

    let compare;
    let result = [];

    for (let p = 0; p < items.length; p++){
        
        if (items[p].stocks == 0){
                
                if (compare == items[p].category){
                    
                    result.push(items[p].category)
                }
        }
        compare = items[p].category;

    }
    return result;
}

function findFlyingVoters(candidateA, candidateB) {
    //Goal:
        // Find voters who voted for both candidate A and candidate B.

    //Array for the test:    
        // The passed values from the test are the following:
        // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
        // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // Expected return must be array:
        // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].

    //Note:
        // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.
    let same = [];

    for (let i = 0; i <= candidateA.length-1; i++){
        console.log(`candidate A: ${i}: ${candidateA[i]}`)
        for (let c = 0; c <= candidateB.length-1; c++){
            console.log(`candidate B: ${c}: ${candidateB[c]}`)
            if (candidateA[i] == candidateB[c]){
                same.push(candidateB[c]);
            }
        }
    }
    return same;
}

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};
